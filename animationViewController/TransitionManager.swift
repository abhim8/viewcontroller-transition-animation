//
//  TransitionManager.swift
//  animationViewController
//
//  Created by Abhishek Patel on 6/24/15.
//  Copyright (c) 2015 Abhishek Patel. All rights reserved.
//

import UIKit

class TransitionManager: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate  {
    
    private var presenting = true

    //Top and Bottom offsets for view with list of bills
    let verticalMargin = CGFloat(60)
    //Left and Right offset for view with list of bills
    let horizontalMargin = CGFloat(30)
    
    //Just edit next coefficient to adjust for your needs
    //Animation for going back time coefficient
    let backgroundAnimationCoefficientTime = 1.0
    //Animation for extension front time coefficient
    let extensionAnimationCoefficientTime = 1.6
    //Delay before firstAnimation
    let delayAnimationCoefficient = 0.1
    
    let springSpeed = 6.0
    
    // MARK: UIViewControllerAnimatedTransitioning protocol methods
    
    // animate a change from one view controller to another
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        // Perform the animation
 
        // get reference to our fromView, toView and the container view that we should perform the transition in
        let container = transitionContext.containerView()
        
        // create a tuple of our screens
        let screens : (from:UIViewController, to:UIViewController) = (transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!, transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!)
        
        // assign references to our menu view controller and the ‘bottom’ view controller from the tuple
        // remember that our second view controller will alternate between the from and to view controller depending if we’re presenting or dismissing
        let toViewController = !self.presenting ? screens.from as UIViewController : screens.to as UIViewController
        let fromViewController = !self.presenting ? screens.to as UIViewController : screens.from as UIViewController
        
        var fromView = fromViewController.view
        var toView = toViewController.view
        
        let fromViewOriginal = fromView
        let toViewOriginal = toView
        
        
        // Set frames and view states before starting animation
        let frameOfFromViewControllerAfterAnimation = CGRect(x: self.horizontalMargin, y: self.verticalMargin, width: CGRectGetWidth(fromView.frame) - (self.horizontalMargin * 2), height: CGRectGetHeight(fromView.frame) - (self.verticalMargin * 2))
        
        let frameOfToViewControllerBeforeAnimation = CGRectMake(0, fromViewOriginal.frame.size.height / 2, fromViewOriginal.frame.width, 0)
        
        toView = toView.snapshotViewAfterScreenUpdates(true)
        fromView = fromView.snapshotViewAfterScreenUpdates(true)
        
        if self.presenting {
            let toViewContainer = UIView(frame: frameOfToViewControllerBeforeAnimation)
            toViewContainer.clipsToBounds = true
            toViewContainer.addSubview(toView)
            toView.frame.origin.y = 0
            toView = toViewContainer
            
            toView.frame = frameOfToViewControllerBeforeAnimation
            toView.alpha = 0.0
        } else {
            let fromViewContainer = UIView(frame: fromView.bounds)
            fromViewContainer.bounds = fromView.bounds
            fromViewContainer.clipsToBounds = true
            fromViewContainer.addSubview(fromView)
            fromView.frame.origin.y = 0
            fromView = fromViewContainer
            
            toView.frame = frameOfFromViewControllerAfterAnimation
            toView.alpha = 0.5
        }
        
        var backgroundView = UIView(frame: container.frame)
        backgroundView.backgroundColor = UIColor.blackColor()
        backgroundView.autoresizingMask = .FlexibleWidth | .FlexibleHeight
        container.addSubview(backgroundView)
        
        container.addSubview(fromView)
        container.addSubview(toView)
        
        if !self.presenting {
            container.bringSubviewToFront(fromView)
        }
        
        // Start animation
        
        if self.presenting {
            var fromViewAlphaAnimation = POPSpringAnimation(propertyNamed: kPOPViewAlpha)
            fromViewAlphaAnimation?.toValue = 0.5
            fromViewAlphaAnimation?.springBounciness = 6
            fromViewAlphaAnimation?.springSpeed = 6
            
            var fromViewFrameAnimation = POPSpringAnimation(propertyNamed: kPOPViewFrame)
            fromViewFrameAnimation?.toValue = NSValue(CGRect: frameOfFromViewControllerAfterAnimation)
            fromViewFrameAnimation?.springBounciness = 6
            fromViewFrameAnimation?.springSpeed = 6
            
            fromView.pop_addAnimation(fromViewAlphaAnimation, forKey: "Alpha Animation")
            fromView.pop_addAnimation(fromViewFrameAnimation, forKey: "Frame Animation")
            
        } else {
            var fromViewAlphaAnimation = POPSpringAnimation(propertyNamed: kPOPViewAlpha)
            fromViewAlphaAnimation?.toValue = 0.8
            fromViewAlphaAnimation?.springBounciness = 6
            fromViewAlphaAnimation?.springSpeed = 6
            
            var fromViewFrameAnimation = POPSpringAnimation(propertyNamed: kPOPViewFrame)
            fromViewFrameAnimation?.toValue = NSValue(CGRect: frameOfToViewControllerBeforeAnimation)
            fromViewFrameAnimation?.springBounciness = 6
            fromViewFrameAnimation?.springSpeed = 6
            
            fromView.pop_addAnimation(fromViewAlphaAnimation, forKey: "Alpha Animation")
            fromView.pop_addAnimation(fromViewFrameAnimation, forKey: "Frame Animation")
        }
        
        var firstPartAnimationDuration = self.transitionDuration(transitionContext)*(self.presenting ? self.backgroundAnimationCoefficientTime : self.extensionAnimationCoefficientTime)
        
        var delayTime = firstPartAnimationDuration - self.delayAnimationCoefficient
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(delayTime*Double(NSEC_PER_SEC))), dispatch_get_main_queue(), { () -> Void in
            
            var toViewFrameAnimation = POPSpringAnimation(propertyNamed: kPOPViewFrame)
            toViewFrameAnimation?.toValue = NSValue(CGRect: fromViewOriginal.frame)
            toViewFrameAnimation?.springBounciness = 6
            toViewFrameAnimation?.springSpeed = 6
            
            toView.pop_addAnimation(toViewFrameAnimation, forKey: "Frame Animation")
            if self.presenting {
                toView.alpha = 0.8
                
                container.addSubview(toViewOriginal)
                toViewOriginal.alpha = 0
                
                var fromViewAlphaAnimation = POPSpringAnimation(propertyNamed: kPOPViewAlpha)
                fromViewAlphaAnimation?.toValue = 0.0
                fromViewAlphaAnimation?.springBounciness = 6
                fromViewAlphaAnimation?.springSpeed = 16
                
                fromView.pop_addAnimation(fromViewAlphaAnimation, forKey: "Alpha Animation")
            } else {
                fromView.alpha = 0.0
            }
    
            toViewFrameAnimation?.completionBlock = {(animation, finished) in
                if finished {
                    
                    var toViewAlphaAnimation = POPSpringAnimation(propertyNamed: kPOPViewAlpha)

                    if self.presenting {
                        toViewAlphaAnimation?.toValue = 1.0
                        toViewAlphaAnimation?.springBounciness = 6
                        toViewAlphaAnimation?.springSpeed = 16
                        
                        var toViewOriginalAlphaAnimation = POPSpringAnimation(propertyNamed: kPOPViewAlpha)
                        toViewOriginalAlphaAnimation?.toValue = 1.0
                        toViewOriginalAlphaAnimation?.springBounciness = 6
                        toViewOriginalAlphaAnimation?.springSpeed = 16
                        
                        toViewOriginal.pop_addAnimation(toViewOriginalAlphaAnimation, forKey: "Alpha Animation")
                        toView.pop_addAnimation(toViewAlphaAnimation, forKey: "Alpha Animation")
                        
                        toViewOriginalAlphaAnimation?.completionBlock = {(animation, finished) in
                            if finished {
                                // tell our transitionContext object that we’ve finished animating
                                backgroundView.removeFromSuperview()
                                
                                toView.removeFromSuperview()
                                fromView.removeFromSuperview()
                                transitionContext.completeTransition(true)
                            }
                        }
                    } else {
                        toViewAlphaAnimation?.toValue = 1.0
                        toViewAlphaAnimation?.springBounciness = 6
                        toViewAlphaAnimation?.springSpeed = 16
                        
                        toView.pop_addAnimation(toViewAlphaAnimation, forKey: "Alpha Animation")
                        
                        // tell our transitionContext object that we’ve finished animating
                        backgroundView.removeFromSuperview()
                        
                        toView.removeFromSuperview()
                        fromView.removeFromSuperview()
                        transitionContext.completeTransition(true)
                    }
                }
            }
        })
    }

    // return how many seconds the transiton animation will take
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return 0.5
    }

    // MARK: UIViewControllerTransitioningDelegate protocol methods

    // return the animator when presenting a view controller
    // remember that an animator (or animation controller) is any object that adheres to the UIViewControllerAnimatedTransitioning protocol
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = true
        return self
    }
    
    // return the animator used when dismissing from a view controller
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = false
        return self
    }
}
