//
//  ViewController.swift
//  animationViewController
//
//  Created by Abhishek Patel on 6/24/15.
//  Copyright (c) 2015 Abhishek Patel. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let theTransistionManager = TransitionManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.transitioningDelegate = theTransistionManager
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func unwindSegueToViewController (sender: UIStoryboardSegue){
        println("Boom shakalaka")
    }

}

